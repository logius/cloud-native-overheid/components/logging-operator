# Logging Operator

This role deploys the [Banzai Logging Operator (BLO)](https://banzaicloud.com/docs/one-eye/logging-operator/) as a CNO component.

## Configuration

### Default Cluster Output

A default cluster output, pointing by default to the in-cluster Elasticsearch instance, is configured as part of the role, by applying a `ClusterOutput` resource along with a `Secret` containing the Elastic password. 

## Generic Logging Flow

A generic flow configuration, to be used in other CNO Components, is available in  the [logging-flow/main.yaml](tasks/logging-flow/main.yml) task file. For most roles, this will be sufficient. If a custom flow is necessary, see template flow below.

It can be integrated as follows:

```
# in meta/main.yml in dependencies
  - name: logging-operator
    src: https://gitlab.com/logius/cloud-native-overheid/components/logging-operator/
    scm: git
    version: master
    tags: [ never, install ] # never tag is required to make sure main.yml in postgres-operator is skipped. 


...
# in tasks/install.yml or tasks.yml, directly under create namespace

- name: create logging flow
  include_role:
    name: logging-operator
    tasks_from: create-generic-flow.yml
  vars: 
    logging_namespace: "{{ config.<insert role name here>.namespace }}"
  when: config.system.use_logging_operator | default (false)

...
```

## Template Flow
A template flow is included in [examples](examples/) that may be used to create custom flows.
It specifies default filters and provides explanation of flows for LPC.

## Example Project

An example `Flow` making use of the configured `ClusterOutput`, including a demo logging generator application, can be found under [examples](examples/).

## Debugging

### Flow FluentD debugging

To get more debugging info from fluentd while building a flow, you can check out the verbose fluentd logging inside the fluentd pod:

```
tail -f /fluentd/log/out
```

In combination with the `stdout` filter this gives you a powerful set of tools to debug complex flows.

```
kind: Flow
spec:
  filters:
  - stdout:
      output_type: json
...
```

### Output Debugging

`GlobalOutputs` can be hard to debug due to the amount of logdata passing through. When dealing with issues with a certain flow / output combination, it can be useful to temporarily add a namespaced `Output`. Using [`ElasticSearch`](https://banzaicloud.com/docs/one-eye/logging-operator/configuration/plugins/outputs/elasticsearch/) as an example, you could set `log_es_400_reason` to see why Elastic is rejecting certain docs (log records). Other useful options are `unrecoverable_error_types`, `with_transporter_log`, see the [Logging Operator's fluentd debugging documentation](https://banzaicloud.com/docs/one-eye/logging-operator/operation/troubleshooting/fluentd/) for more details. 


### Resources and Problems

By listing `Flow` / `Output` / ``ClusterOutput`` resources using kubectl, you can see if your configured resources have been processed correctly by the operator. By describing resources with problems, you can get some guidance as to where the problem in your logging configuration may lie.

```
kubectl get flows -o wide
kubectl get output -o wide
kubectl get clusteroutput -n l12m-logging -o wide
```


### Configcheck Fails

####  `...in 'skip': invalid byte sequence in UTF-8 (ArgumentError)`

In this case, it is likely one or more resources in the configuration contains non-ascii characters. This is often caused by improperly base64 encoded secrets used by (cluster) outputs, which end up in the FluentD configuration. 

As the configmap is not regenerated on every resource (`logging` / `flow` / `output` / `clusteroutput` / `secret` / `configmap`), the resource change triggering the configcheck may not be the resource causing the underlying issue.

### Patching

When any flow or output is misconfigured, this blocks any new configuration, including patches. You can check for misconfiguration by observing the resources (see [Resources and Problems](#Resources and Problems)), by checking the Logging Operator pod logs, or by checking the version of fluentd and fluentbit pods. Removing falsive flows and outputs should immediately correct the problem and prompt configuration updates. Deleting flows and outputs requires container admin privileges, however.
