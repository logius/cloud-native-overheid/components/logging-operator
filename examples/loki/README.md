## Namespaced output with Loki

This example will result in an example app logging to a namespaced output, in this case an instance of Loki deployed using the [CNO role](https://gitlab.com/logius/cloud-native-overheid/components/loki). Be mindful of the namespace you use when deploying the test-app, since some changes might be neccesary.

An example logging generator demo app is provided by Banzai which can be deployed using provided [deployment](log-generator-deployment.yml). It can be used in combination with the [example flow](demo-flow.yml) provided in this repository.

```
kubectl create ns lpctest-logging-demo
kubectl apply -n lpctest-logging-demo -f loki-output.yml
kubectl apply -n lpctest-logging-demo -f loki-flow.yml
kubectl apply -n lpctest-logging-demo -f log-generator-deployment.yml
```

The resulting logging can be observed in the UI of Grafana by adding the instance of Loki as a datasource to any Grafana instance.
