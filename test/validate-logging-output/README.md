# How to test

Install kyverno cli, e.g. download from release page https://github.com/kyverno/kyverno/releases.

Run in this directory `kyverno test .` 

The output should be as follows. All results should report `Pass`.

````
│───│─────────────────────────│──────────────────────────│───────────────────────────────────────────│────────│
│ # │ POLICY                  │ RULE                     │ RESOURCE                                  │ RESULT │
│───│─────────────────────────│──────────────────────────│───────────────────────────────────────────│────────│
│ 1 │ validate-logging-output │ check-thread-count       │ default/Output/invalid-thread-count       │ Pass   │
│ 2 │ validate-logging-output │ check-flush-mode         │ default/Output/invalid-flush-mode         │ Pass   │
│ 3 │ validate-logging-output │ check-limit-size         │ default/Output/invalid-chunk-limit-size   │ Pass   │
│ 4 │ validate-logging-output │ check-reconnect-on-error │ default/Output/invalid-reconnect-on-error │ Pass   │
│ 5 │ validate-logging-output │ check-reload-on-failure  │ default/Output/invalid-reload-on-failure  │ Pass   │
│ 6 │ validate-logging-output │ check-reload-connections │ default/Output/invalid-reload-connections │ Pass   │
│ 7 │ validate-logging-output │ check-thread-count       │ default/Output/not-elastic-output         │ Pass   │
│ 8 │ validate-logging-output │ check-thread-count       │ default/Output/valid-output               │ Pass   │
│ 9 │ validate-logging-output │ check-flush-mode         │ default/Output/valid-output               │ Pass   │
│───│─────────────────────────│──────────────────────────│───────────────────────────────────────────│────────│
````